# Nginx Lets Encrypt Proxy

A docker compose project that provides an ssl enabled nginx http proxy with lets encrypt

# Running
1.  Create a custom environment file, e.g local.env <br/>
`CONFIG_CONTACT_EMAIL=user@example.com`<br/>
`CONFIG_PROXY_TO=http://172.17.0.1:3000`<br/>
`CONFIG_FQDN=test.example.com`<br/>
2.  Run the container
`docker-compose --env-file local.env up`
3.  Optional - Configure your firewall to allow access to ports 80, 443 (Optional)

# Configuration File
The container requires the following environment variables to be set

*  CONFIG_CONTACT_EMAIL this is where lets encrypt will send notifications 
*  CONFIG_PROXY_TO uses nginx proxy-pass should supports any url nginx proxy pass does
*  CONFIG_FQDN  this is the domain that the certificate will be generated for

The repository by default ignores files ending in .env  



# Networking
In order for lets encrypt to generate a valid certificate your host **must** meet the following requirements 

*  Host must have a publicly resolvable domain name
*  Host must be publicly reachable on port 80
